﻿Netcube-Switch05#sh run
Building configuration...

Current configuration : 23866 bytes
!
! Last configuration change at 09:50:54 AEST Fri Aug 26 2016 by tvb-netcube-app
! NVRAM config last updated at 09:50:57 AEST Fri Aug 26 2016 by tvb-netcube-app
!
version 12.2
no service pad
service timestamps debug datetime localtime
service timestamps log datetime localtime
no service password-encryption
!
hostname Netcube-Switch05
!
boot-start-marker
boot-end-marker
!
logging buffered 2147483
logging console alerts
enable secret 5 $1$ejXE$j2m061wsDOJ/uS7ZGA4JQ.
!
username tvb-netcube-app privilege 15 password 7 005C2357474C2B250C0118
username renli privilege 15 secret 5 $1$WTkV$z4.6Qftec9eevHKucrB5v0
username nick privilege 15 secret 5 $1$vYx6$nuKa0K1ox9Ut7Vfh7Y8QU1
username novateladmin privilege 15 secret 5 $1$164L$q9DzTYzAh/ydyRvN7tkV51
username liren privilege 15 secret 5 $1$g6ET$xwBU3ms0nwlblknMQ/U69/
!
!
no aaa new-model
clock timezone AEST 10
clock summer-time AEDT recurring 1 Sun Oct 2:00 1 Sun Apr 3:00
switch 1 provision ws-c3750x-24
switch 2 provision ws-c3750x-24
system mtu routing 1500
ip routing
ip dhcp excluded-address 192.168.3.1 192.168.3.3
ip dhcp excluded-address 192.168.3.4 192.168.3.5
ip dhcp excluded-address 192.168.3.211 192.168.3.212
ip dhcp excluded-address 192.168.3.51
ip dhcp excluded-address 10.10.15.1 10.10.15.10
ip dhcp excluded-address 10.10.15.253 10.10.15.254
!
ip dhcp pool V1500
   network 192.168.3.0 255.255.255.0
   default-router 192.168.3.1 
   dns-server 8.8.8.8 
   lease 7
!
ip dhcp pool V104
   network 10.10.15.0 255.255.255.0
   dns-server 8.8.8.8 
   default-router 10.10.15.1 
   lease 7
!
!
no ip domain-lookup
vtp mode transparent
!
mls qos
!
crypto pki trustpoint TP-self-signed-454194688
 enrollment selfsigned
 subject-name cn=IOS-Self-Signed-Certificate-454194688
 revocation-check none
 rsakeypair TP-self-signed-454194688
!
!
crypto pki certificate chain TP-self-signed-454194688
 certificate self-signed 01
  30820247 308201B0 A0030201 02020101 300D0609 2A864886 F70D0101 04050030 
  30312E30 2C060355 04031325 494F532D 53656C66 2D536967 6E65642D 43657274 
  69666963 6174652D 34353431 39343638 38301E17 0D393330 33303130 30303133 
  325A170D 32303031 30313030 30303030 5A303031 2E302C06 03550403 1325494F 
  532D5365 6C662D53 69676E65 642D4365 72746966 69636174 652D3435 34313934 
  36383830 819F300D 06092A86 4886F70D 01010105 0003818D 00308189 02818100 
  C42940A9 7CCCD024 B0CC2CC5 86BBC5E0 9EBEF620 04648659 B328E818 4BB55106 
  77C4C763 00A4C685 8C59E622 68143CA7 272EC14A 118257D8 69A83FFE 5088383F 
  920C0743 34BE1C5F 070E0736 4DC5B232 38F42BD8 30E9FC03 D7D411B3 C026EDB9 
  B6C396E4 1ABD4A1D 7E15AA87 22A7E90E 4B8EE822 47606BC9 99DA53DC 8107DA39 
  02030100 01A37130 6F300F06 03551D13 0101FF04 05300301 01FF301C 0603551D 
  11041530 1382114E 65746375 62652D53 77697463 6830352E 301F0603 551D2304 
  18301680 14CE36DD 36DEC6D9 AC13E100 328BD35D 5F0F93BA 1D301D06 03551D0E 
  04160414 CE36DD36 DEC6D9AC 13E10032 8BD35D5F 0F93BA1D 300D0609 2A864886 
  F70D0101 04050003 818100A6 5A9CE4BB CF7DC42C 75DF86BA DFC50AD0 08490090 
  4EB37614 9C68634B 50DFC6C9 3E450CBD D6139818 D47FCA9E D9FB2422 BFCF0347 
  7DFDA7E3 F35CD3C1 2CC98433 80AC5A6D BA6D426B EDAD7ADF 2984491B 62CE91B1 
  886DF3FD 59D41B54 507EC1BD 45F2E058 5A157916 A4A03D8A 50D929A2 5EB299B5 
  9767CBAA 8C7D8D91 383EA5
  quit
archive
 path ftp://3.3.3.2/$h.cfg
 write-memory
 time-period 1440
spanning-tree mode pvst
spanning-tree extend system-id
!
!
!
port-channel load-balance src-dst-ip
!
vlan internal allocation policy ascending
!
vlan 5
 name L3_OSPF_ASR_SW5
!
vlan 6
 name L3_OSPF_SW5_R01_EQX
!
vlan 10   
 name EQX-IP-PRIM
!
vlan 11
 name EQX-IP-BACKUP
!
vlan 12
 name EQX-PEERING
!
vlan 50
 name Rspan
 remote-span
!
vlan 100-101 
!
vlan 102
 name EQUINIX-P2P
!
vlan 103
 name HWFW-MGMT
!
vlan 104-105 
!
vlan 112
 name P2PfromEquinixt-1
!
vlan 420
 name OPTI_NBN_CVC
!
vlan 428
 name TO-EFTEL-AGVC
!
vlan 605
 name To-AAPT-AGVC
!
vlan 606
 name AAPT-CTS
!
vlan 608 
!
vlan 661
 name PIPE-data-clients
!
vlan 662
 name PIPE-call_termination
!
vlan 663
 name AAPT_SIP
!
vlan 700
 name P2P-TO-PIPE
!
vlan 701
 name MarHome100MPPPoE
!
vlan 702
 name Brisbane50M
!
vlan 703
 name David-Chen-PPPOE
!
vlan 704
 name Seasons-Hotel-SHP
!
vlan 705
 name Seasons-Hotel-SDH
!
vlan 706
 name Seasons-Hotel-SBG
!
vlan 707
 name Seasons-Hotel-SOP
!         
vlan 708
 name Seasons-Hotel-SHM
!
vlan 709
 name City_Discount
!
vlan 710
 name Qing-Chen-20M
!
vlan 711
 name flagexplore
!
vlan 800
 name To-AAPT-IP-Transit
!
vlan 801
 name APT-IP-FRom-HWFW
!
vlan 1000
 name rack-vlan1000
!
vlan 1010
 name EQX-FW-ASR
!
vlan 1012
 name EQX-PEERING-FW-ASR
!
vlan 1025
 name rack-vlan1025
!
vlan 1050
 name rack-vlan1050
!
vlan 1051
 name rack-vlan1051
!         
vlan 1100
 name VM-management
!
vlan 1200
 name To-Colocation-Server
!
vlan 1300
 name mgmt
!
vlan 1500
 name Management
!
vlan 1504 
!
vlan 1600
 name Storage
!
vlan 1700
 name vMotion
!
vlan 1800
 name Provisoning
!
vlan 1994
 name TO-EFTEL-3G-AGVC
!
vlan 1995
 name To-EFTEL-IP-Transit
!
ip ftp source-interface Vlan1600
ip ftp username configbackup
ip ftp password NET888cube
!
class-map match-all PBX-out
 match access-group name PBX-out
class-map match-all PBX-in
 match access-group name PBX-in
!
!
policy-map in
 class PBX-in
  police 450000000 1000000 exceed-action drop
 class class-default
policy-map out
 class PBX-out
  police 450000000 1000000 exceed-action drop
 class class-default
!
!
!
interface Loopback100
 description Router ID
 ip address 103.26.63.129 255.255.255.255
 ip ospf 1 area 0
!
interface Port-channel1
 description to Netcube-Switch01
 switchport trunk encapsulation dot1q
 switchport trunk allowed vlan 101-105,112,428,605,606,608,661-663,700,702,800
 switchport trunk allowed vlan add 801,1000,1025,1050,1051,1100,1200,1300,1400
 switchport trunk allowed vlan add 1500,1600,1700,1800,1994,1995
 switchport mode trunk
!
interface Port-channel10
 description TO-ASR1002-X
 switchport trunk encapsulation dot1q
 switchport trunk allowed vlan 1,5,105,700,701,707-710,1100,1300,1500,1504,1994
 switchport trunk allowed vlan add 1995,4001-4094
 switchport mode trunk
 switchport nonegotiate
 spanning-tree portfast trunk
!         
interface Port-channel11
 description TO-HWFW-IP-TO-NOVATEL
 switchport access vlan 801
 switchport mode access
!
interface Port-channel12
 description HWFW-FROM-AAPT-IP
 switchport access vlan 800
 switchport mode access
!
interface Port-channel13
 description P2PfromEquinix
 switchport access vlan 10
 switchport mode access
!
interface Port-channel14
 description P2PfromEquinix-NOVATEL
 switchport access vlan 1010
 switchport mode access
!
interface Port-channel15
 description P2PfromEquinix-peering
 switchport access vlan 12
 switchport mode access
!
interface Port-channel16
 description P2PfromEquinix-peering-NOVATEL
 switchport access vlan 1012
 switchport mode access
!
interface FastEthernet0
 no ip address
 no ip route-cache cef
 no ip route-cache
 no ip mroute-cache
 shutdown 
!
interface GigabitEthernet1/0/1
 description Uplink To ASR1002x (Gig 0/0/0) [Po1]
 switchport trunk encapsulation dot1q
 switchport trunk allowed vlan 1,5,105,700,701,707-710,1100,1300,1500,1504,1994
 switchport trunk allowed vlan add 1995,4001-4094
 switchport mode trunk
 switchport nonegotiate
 channel-group 10 mode active
!
interface GigabitEthernet1/0/2
 description Uplink To ASR1002x (Gig 0/0/1) [Po1]
 switchport trunk encapsulation dot1q
 switchport trunk allowed vlan 1,5,105,700,701,707-710,1100,1300,1500,1504,1994
 switchport trunk allowed vlan add 1995,4001-4094
 switchport mode trunk
 switchport nonegotiate
 channel-group 10 mode active
!
interface GigabitEthernet1/0/3
 description Uplink To ASR1002x (Gig 0/0/2) [AAPT Transit Interface]
 switchport trunk encapsulation dot1q
 switchport trunk allowed vlan 800,801
 switchport mode trunk
 switchport nonegotiate
 service-policy input out
!
interface GigabitEthernet1/0/4
 description Uplink To ASR1002x (Gig 0/0/3) [Equinix Transit Interface]
 switchport trunk encapsulation dot1q
 switchport trunk allowed vlan 10,1010
 switchport mode trunk
 switchport nonegotiate
!
interface GigabitEthernet1/0/5
 description to PIPE-client_and_CTS dot1q trunk on NTU
 switchport trunk encapsulation dot1q
 switchport trunk allowed vlan 661,662
 switchport mode trunk
 duplex full
!
interface GigabitEthernet1/0/6
 description to PIPE-office dot1q trunk on NTU
 switchport trunk encapsulation dot1q
 switchport trunk allowed vlan 700
 switchport mode trunk
 load-interval 30
 duplex full
!
interface GigabitEthernet1/0/7
 description Provider - AAPT (Transit + P2P Links) (CID:1277563)
 switchport trunk encapsulation dot1q
 switchport trunk allowed vlan 606,701,800
 switchport mode trunk
 speed 1000
 duplex full
!
interface GigabitEthernet1/0/8
 description to 7206 Router Trunk port Gig0/1
 switchport trunk encapsulation dot1q
 switchport mode trunk
!
interface GigabitEthernet1/0/9
 description Uplink To switch2
 switchport trunk encapsulation dot1q
 switchport trunk allowed vlan 103
 switchport mode trunk
!
interface GigabitEthernet1/0/10
 description Uplink To ASR1002x (Gig 0/0/4) [Equinix Peering Interface]
 switchport trunk encapsulation dot1q
 switchport trunk allowed vlan 12,1012
 switchport mode trunk
 spanning-tree portfast trunk
!
interface GigabitEthernet1/0/11
 description TPG First dot1q trunk 
 switchport trunk encapsulation dot1q
 switchport trunk allowed vlan 104,105
 switchport mode trunk
 load-interval 30
 duplex full
!
interface GigabitEthernet1/0/12
 description TPG Ssecond dot1q trunk
 switchport trunk encapsulation dot1q
 switchport trunk allowed vlan 104,105
 switchport mode trunk
 load-interval 30
 duplex full
!
interface GigabitEthernet1/0/13
!
interface GigabitEthernet1/0/14
 description PORT-MIRROR
!
interface GigabitEthernet1/0/15
 description Uplink To ASR1002x (Gig 0/0/5) [Po1]
 switchport trunk encapsulation dot1q
 switchport trunk allowed vlan 1,5,105,700,701,707-710,1100,1300,1500,1504,1994
 switchport trunk allowed vlan add 1995,4001-4094
 switchport mode trunk
 switchport nonegotiate
 channel-group 10 mode active
!
interface GigabitEthernet1/0/16
!
interface GigabitEthernet1/0/17
 description to PIX515E-swtich3750-02
 switchport access vlan 1200
 switchport mode access
!
interface GigabitEthernet1/0/18
 description to ASR1002X-Management
 switchport access vlan 101
 switchport mode access
!
interface GigabitEthernet1/0/19
 description to Huawei-FW-MGMT
 switchport access vlan 1995
 switchport mode access
 spanning-tree portfast
!
interface GigabitEthernet1/0/20
 description to Netcube-Switch01
 switchport trunk encapsulation dot1q
 switchport trunk allowed vlan 101-105,112,428,605,606,608,661-663,700,702,800
 switchport trunk allowed vlan add 801,1000,1025,1050,1051,1100,1200,1300,1400
 switchport trunk allowed vlan add 1500,1600,1700,1800,1994,1995
 switchport mode trunk
 channel-group 1 mode on
!
interface GigabitEthernet1/0/21
 description to Netcube-Switch01
 switchport trunk encapsulation dot1q
 switchport trunk allowed vlan 101-105,112,428,605,606,608,661-663,700,702,800
 switchport trunk allowed vlan add 801,1000,1025,1050,1051,1100,1200,1300,1400
 switchport trunk allowed vlan add 1500,1600,1700,1800,1994,1995
 switchport mode trunk
 channel-group 1 mode on
!
interface GigabitEthernet1/0/22
 description to Netcube-Switch01
 switchport trunk encapsulation dot1q
 switchport trunk allowed vlan 101-105,112,428,605,606,608,661-663,700,702,800
 switchport trunk allowed vlan add 801,1000,1025,1050,1051,1100,1200,1300,1400
 switchport trunk allowed vlan add 1500,1600,1700,1800,1994,1995
 switchport mode trunk
 channel-group 1 mode on
!
interface GigabitEthernet1/0/23
 description to Netcube-Switch01
 switchport trunk encapsulation dot1q
 switchport trunk allowed vlan 101-105,112,428,605,606,608,661-663,700,702,800
 switchport trunk allowed vlan add 801,1000,1025,1050,1051,1100,1200,1300,1400
 switchport trunk allowed vlan add 1500,1600,1700,1800,1994,1995
 switchport mode trunk
 channel-group 1 mode on
!
interface GigabitEthernet1/0/24
 description to Netcube-Switch01
 switchport trunk encapsulation dot1q
 switchport trunk allowed vlan 101-105,112,428,605,606,608,661-663,700,702,800
 switchport trunk allowed vlan add 801,1000,1025,1050,1051,1100,1200,1300,1400
 switchport trunk allowed vlan add 1500,1600,1700,1800,1994,1995
 switchport mode trunk
 channel-group 1 mode on
!
interface GigabitEthernet1/1/1
!
interface GigabitEthernet1/1/2
!
interface GigabitEthernet1/1/3
!
interface GigabitEthernet1/1/4
!
interface TenGigabitEthernet1/1/1
 description Provider - AAPT (AGVC) (CID:1122754)
 switchport trunk encapsulation dot1q
 switchport trunk allowed vlan 104,105,605,608,635,663,702-707,709-711,1500
 switchport mode trunk
 speed nonegotiate
 service-policy input in
!
interface TenGigabitEthernet1/1/2
 description Uplink To ASR1002x (TenGigX/X)
 switchport trunk encapsulation dot1q
 switchport trunk allowed vlan 10,103-105,420,428,605,608,661,700,701,704-710
 switchport trunk allowed vlan add 1000,1051,1100,1200,1300,1500,1504,1994,1995
 switchport mode trunk
 switchport nonegotiate
 spanning-tree portfast trunk
!
interface GigabitEthernet2/0/1
 description HWFW-G1/0/0
 switchport access vlan 801
 switchport mode access
 no cdp enable
 channel-group 11 mode on
!
interface GigabitEthernet2/0/2
 description HWFW-G3/0/0
 switchport access vlan 801
 switchport mode access
 no cdp enable
 channel-group 11 mode on
!
interface GigabitEthernet2/0/3
 description HWFW-G1/0/1
 switchport access vlan 800
 switchport mode access
 no cdp enable
 channel-group 12 mode on
!
interface GigabitEthernet2/0/4
 description HWFW-G3/0/1
 switchport access vlan 800
 switchport mode access
 no cdp enable
 channel-group 12 mode on
!
interface GigabitEthernet2/0/5
 description HWFW-G1/0/2
 switchport access vlan 10
 switchport mode access
 no cdp enable
 channel-group 13 mode on
!
interface GigabitEthernet2/0/6
 description HWFW-G3/0/2
 switchport access vlan 10
 switchport mode access
 no cdp enable
 channel-group 13 mode on
!
interface GigabitEthernet2/0/7
 description HWFW-G1/0/3
 switchport access vlan 12
 switchport mode access
 channel-group 15 mode on
!
interface GigabitEthernet2/0/8
 description HWFW-G3/0/3
 switchport access vlan 12
 switchport mode access
 channel-group 15 mode on
!
interface GigabitEthernet2/0/9
 description HWFW-G1/0/4
 switchport access vlan 1010
 switchport mode access
 no cdp enable
 channel-group 14 mode on
!
interface GigabitEthernet2/0/10
 description HWFW-G3/0/4
 switchport access vlan 1010
 switchport mode access
 no cdp enable
 channel-group 14 mode on
!
interface GigabitEthernet2/0/11
 description HWFW-G1/0/5
 switchport access vlan 1012
 switchport mode access
 channel-group 16 mode on
!
interface GigabitEthernet2/0/12
 description HWFW-G3/0/5
 switchport access vlan 1012
 switchport mode access
 channel-group 16 mode on
!
interface GigabitEthernet2/0/13
 description LUKE-LAPTOP-REMOTE-CONSOLE
 switchport access vlan 1000
 switchport mode access
 spanning-tree portfast
!
interface GigabitEthernet2/0/14
 description test
 switchport access vlan 103
 switchport mode access
 load-interval 30
!
interface GigabitEthernet2/0/15
!
interface GigabitEthernet2/0/16
!
interface GigabitEthernet2/0/17
!
interface GigabitEthernet2/0/18
!
interface GigabitEthernet2/0/19
!
interface GigabitEthernet2/0/20
 switchport access vlan 1500
 switchport mode access
 spanning-tree portfast
!
interface GigabitEthernet2/0/21
!
interface GigabitEthernet2/0/22
!
interface GigabitEthernet2/0/23
!
interface GigabitEthernet2/0/24
!
interface GigabitEthernet2/1/1
 description to EFTel dot1q trunk port M1-2A-02-35-01
 switchport trunk encapsulation dot1q
 switchport trunk allowed vlan 428,1500,1994,1995
 switchport mode trunk
 switchport nonegotiate
!
interface GigabitEthernet2/1/2
 description InterPoP LInk To Equinix MEL (R01) (Mircon21:130417)
 switchport trunk encapsulation dot1q
 switchport trunk allowed vlan 6,10-12,101-103,420,1500,1504,1600,1700,1800
 switchport mode trunk
 load-interval 30
!
interface GigabitEthernet2/1/3
 description Provider - Teltra 1G Truck (TelstraRef:197653925)
 switchport trunk encapsulation dot1q
 switchport trunk allowed vlan 708
 switchport mode trunk
!
interface GigabitEthernet2/1/4
 description to EFTel dot1q trunk port M1-2A-02-35-01
 switchport trunk encapsulation dot1q
 switchport trunk allowed vlan 428,1500,1994,1995
 switchport mode trunk
 switchport nonegotiate
!
interface TenGigabitEthernet2/1/1
 description InterPoP LInk To Equinix MEL (R01) (Mircon21:130417)
 switchport trunk encapsulation dot1q
 switchport trunk allowed vlan 6,10-12,50,101-103,420,1500,1504,1600,1700,1800
 switchport mode trunk
 load-interval 30
!
interface TenGigabitEthernet2/1/2
!
interface Vlan1
 no ip address
 shutdown
!
interface Vlan5
 description L3 To ASR (OSPF)
 ip address 103.26.63.133 255.255.255.254
 ip ospf network point-to-point
 ip ospf 1 area 0
!
interface Vlan6
 description L3 To R01 [Equinix] (OSPF)
 ip address 103.26.63.135 255.255.255.254
 ip ospf network point-to-point
 ip ospf 1 area 0
!         
interface Vlan101
 ip address 192.168.0.103 255.255.255.0
!
interface Vlan103
 description Public-Management-Netwrok
 ip address 43.252.111.22 255.255.255.248
!
interface Vlan104
 ip address 10.10.15.3 255.255.255.0
!
interface Vlan105
 ip address 10.10.16.3 255.255.255.0
!
interface Vlan1300
 description MGMT
 ip address 103.26.62.5 255.255.255.192
!
interface Vlan1400
 description vMotion
 ip address 1.1.1.254 255.255.255.0
!
interface Vlan1500
 ip address 192.168.3.2 255.255.255.0
!
interface Vlan1504
 ip address 192.168.4.3 255.255.255.0
!
interface Vlan1600
 ip address 3.3.3.45 255.255.0.0
!
router ospf 1
 router-id 103.26.63.129
 ispf
 log-adjacency-changes
 timers throttle spf 1 2000 2000
 timers throttle lsa all 1 2000 2000
 passive-interface default
 no passive-interface Vlan5
 no passive-interface Vlan6
!
ip classless
ip route 0.0.0.0 0.0.0.0 103.26.62.1
ip http server
ip http secure-server
!
ip access-list standard snmp-in
 permit 173.236.108.29
 permit 14.137.150.43
 permit 14.137.150.83
 permit 203.12.160.5
 permit 43.252.111.51
 permit 54.253.115.17
 permit 172.29.0.3
 permit 172.29.0.4
 permit 172.29.0.10
 permit 54.252.97.16
 permit 192.168.3.168
!
ip access-list extended PBX-in
 deny   ip any host 103.26.62.252
 permit ip any any
ip access-list extended PBX-out
 deny   ip host 103.26.62.252 any
 permit ip any any
!
kron occurrence backup at 23:00 recurring
 policy-list backup
!
kron occurrence BACKUP_OCCURRENCE at 23:00 recurring
 policy-list BACKUP
!
kron policy-list backup
!
kron policy-list BACKUP
 cli write memory
!
access-list 17 permit 203.111.96.209
access-list 17 permit 120.149.176.111
access-list 17 permit 120.149.176.8
access-list 17 permit 124.188.201.118
access-list 17 permit 1.136.97.95
access-list 17 permit 14.137.113.77
access-list 17 permit 54.252.97.16
access-list 17 permit 203.123.82.1
access-list 17 permit 192.168.3.0 0.0.0.255
access-list 17 permit 192.168.0.0 0.0.255.255
access-list 17 permit 14.137.150.0 0.0.0.255
access-list 17 permit 43.252.108.0 0.0.3.255
access-list 17 permit 103.26.60.0 0.0.3.255
access-list 17 permit 103.58.216.0 0.0.3.255
access-list 17 permit 103.249.140.0 0.0.3.255
access-list 17 permit 103.18.48.0 0.0.3.255
access-list 17 permit 119.31.228.0 0.0.0.255
access-list 42 permit 192.168.3.0 0.0.0.255
snmp-server group v3user v3 auth notify *tv.FFFFFFFF.FFFFFFFF.FFFFFFFF.FFFFFFFF7F 
snmp-server group V3Group v3 auth notify *tv.FFFFFFFF.FFFFFFFF.FFFFFFFF.FFFFFFFF7F 
snmp-server group netcubegroup v3 auth notify *tv.FFFFFFFF.FFFFFFFF.FFFFFFFF.FFFFFFFF7F 
snmp-server group netcube-v3-group v3 auth notify *tv.FFFFFFFF.FFFFFFFF.FFFFFFFF.FFFFFFFF7F 
snmp-server group netcube-v3-group v3 priv read netcube-v3-read write netcube-v3-write access snmp-in
snmp-server group netcube-cisco-v3-group v3 auth read netcube-cisco-v3-read write netcube-cisco-v3-write notify *tv.FFFFFFFF.FFFFFFFF.FFFFFFFF.FFFFFFFF7F 
snmp-server view netcube-v3-read iso included
snmp-server view netcube-v3-write iso included
snmp-server view netcube-cisco-v3-read iso included
snmp-server view netcube-cisco-v3-write iso included
snmp-server community H05t8IkkW79TE RO snmp-in
snmp-server community netcube RW netcuberw
snmp-server community private RW
snmp-server community public RO
snmp-server community netcubero RO
snmp-server community netcubewr RW
snmp-server community NETCUBEread RO 42
snmp-server enable traps snmp authentication linkdown linkup coldstart warmstart
snmp-server enable traps transceiver all
snmp-server enable traps tty
snmp-server enable traps eigrp
snmp-server enable traps ospf state-change
snmp-server enable traps ospf errors
snmp-server enable traps ospf retransmit
snmp-server enable traps ospf lsa
snmp-server enable traps ospf cisco-specific state-change nssa-trans-change
snmp-server enable traps ospf cisco-specific state-change shamlink interface-old
snmp-server enable traps ospf cisco-specific state-change shamlink neighbor
snmp-server enable traps ospf cisco-specific errors
snmp-server enable traps ospf cisco-specific retransmit
snmp-server enable traps ospf cisco-specific lsa
snmp-server enable traps license
snmp-server enable traps auth-framework sec-violation
snmp-server enable traps cef resource-failure peer-state-change peer-fib-state-change inconsistency
snmp-server enable traps cluster
snmp-server enable traps config-copy
snmp-server enable traps config
snmp-server enable traps config-ctid
snmp-server enable traps dot1x auth-fail-vlan guest-vlan no-auth-fail-vlan no-guest-vlan
snmp-server enable traps energywise
snmp-server enable traps fru-ctrl
snmp-server enable traps entity
snmp-server enable traps event-manager
snmp-server enable traps hsrp
snmp-server enable traps ipmulticast
snmp-server enable traps power-ethernet group 1-9
snmp-server enable traps power-ethernet police
snmp-server enable traps pim neighbor-change rp-mapping-change invalid-pim-message
snmp-server enable traps cpu threshold
snmp-server enable traps rtr
snmp-server enable traps vstack
snmp-server enable traps bridge newroot topologychange
snmp-server enable traps stpx inconsistency root-inconsistency loop-inconsistency
snmp-server enable traps syslog
snmp-server enable traps vtp
snmp-server enable traps vlancreate
snmp-server enable traps vlandelete
snmp-server enable traps flash insertion removal
snmp-server enable traps port-security
snmp-server enable traps envmon fan shutdown supply temperature status
snmp-server enable traps stackwise
snmp-server enable traps errdisable
snmp-server enable traps mac-notification change move threshold
snmp-server enable traps vlan-membership
snmp-server host 10.0.10.211 version 3 auth V3User 
snmp-server host 192.168.3.2 version 3 auth V3User 
snmp-server host 192.168.3.156 version 3 auth netcube-cisco-v3-user 
snmp-server host 192.168.3.168 version 3 auth netcube-cisco-v3-user 
snmp-server host 192.168.3.168 version 3 auth netcube-v3-user 
snmp-server host 192.168.3.156 netcube 
snmp-server host 192.168.3.156 netcubero 
snmp-server host 192.168.3.156 version 3 auth netcubeuser 
snmp-server host 192.168.3.168 version 3 auth v3user 
!
!
line con 0
 logging synchronous
line vty 0 4
 session-timeout 30 
 access-class 17 in
 exec-timeout 30 0
 password 7 110A1016141D5A5E577B7977
 logging synchronous
 login local
 transport preferred ssh
 transport input ssh
line vty 5 15
 session-timeout 30 
 access-class 17 in
 exec-timeout 30 0
 password 7 110A1016141D5A5E577B7977
 logging synchronous
 login local
 transport preferred ssh
 transport input ssh
!
!
monitor session 1 source vlan 10 , 12 , 800 tx
monitor session 1 destination remote vlan 50 
monitor session 2 source interface Po10
monitor session 2 destination interface Gi2/0/14 encapsulation replicate
ntp clock-period 36027437
ntp server 202.127.210.36
end

