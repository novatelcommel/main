﻿EQUINIX-Edge-SW01#sh run
Building configuration...

Current configuration : 12155 bytes
!
! No configuration change since last restart
! NVRAM config last updated at 22:59:56 AEST Thu Aug 25 2016
!
version 15.0
no service pad
service timestamps debug datetime msec
service timestamps log datetime msec
no service password-encryption
!
hostname EQUINIX-Edge-SW01
!
boot-start-marker
boot-end-marker
!
!
logging buffered 2147483
enable secret 4 kITT02qPfio0AZwKCXCxr8tqGPxx7RN72992g7vMbWk
!
username tvb-netcube-app secret 4 ZdkDEVu2qk9l9psFDEwzenoBXmIFVbSZnV30R75BZQc
username nick privilege 15 secret 4 XrzXC8VwQvO0pXc2FllC51vWCvGav2AXVtZtZlaw3Zs
username novateladmin privilege 15 secret 4 B0KrTsePlNcLw5Y7TbVu33H5DJ/288gljGdA4ZJ8gV6
username renli privilege 15 secret 4 tWWUKLwGTysSGJvh1UHd50.mYq8poflwTu097M8n/eM
no aaa new-model
clock timezone AEST 10 0
clock summer-time AEDT recurring 1 Sun Oct 2:00 1 Sun Apr 3:00
switch 1 provision ws-c3750x-24
system mtu routing 1500
!
!
!
no ip domain-lookup
ip domain-name CloudComm.com.au
vtp mode transparent
!         
!
crypto pki trustpoint TP-self-signed-3937114624
 enrollment selfsigned
 subject-name cn=IOS-Self-Signed-Certificate-3937114624
 revocation-check none
 rsakeypair TP-self-signed-3937114624
!
!
crypto pki certificate chain TP-self-signed-3937114624
 certificate self-signed 01
  3082022B 30820194 A0030201 02020101 300D0609 2A864886 F70D0101 05050030 
  31312F30 2D060355 04031326 494F532D 53656C66 2D536967 6E65642D 43657274 
  69666963 6174652D 33393337 31313436 3234301E 170D3131 30333330 30313239 
  30395A17 0D323030 31303130 30303030 305A3031 312F302D 06035504 03132649 
  4F532D53 656C662D 5369676E 65642D43 65727469 66696361 74652D33 39333731 
  31343632 3430819F 300D0609 2A864886 F70D0101 01050003 818D0030 81890281 
  8100A004 D0E3F06E 914B9BA5 431C4C70 91177CB1 EDE2055E 226899D1 B3733A4F 
  6A52E24B 600FD816 9DA365E5 84574192 1E88128A 745137CB A871A08D 5F5E117C 
  DD96CB5B D0EF4848 F8956149 555F5F28 6EFAF0C9 AF70FFC7 F00DEAE3 2534C0AE 
  B2F16232 0348A1F2 24766848 D9CA59F5 A6873E0F 7E7D7B60 6F2474AC 88A92AF2 
  AE2D0203 010001A3 53305130 0F060355 1D130101 FF040530 030101FF 301F0603 
  551D2304 18301680 1448A8CD 113D555F 587BC654 EC2E3CD4 E737E430 87301D06 
  03551D0E 04160414 48A8CD11 3D555F58 7BC654EC 2E3CD4E7 37E43087 300D0609 
  2A864886 F70D0101 05050003 81810036 61FE212F 3733C3C5 64A05751 775D0EE5 
  862616EE B9E43855 B75816B0 99BF4C6E 9906259D CA6133A4 7D49F135 01C0073B 
  6D2A8F27 8FD8C33A F18F9943 EFE3246E CE3D17D3 24AC78E3 BEF462A5 9290F852 
  E734BCDD D19D8ECF F64F5C13 38AEA237 FADBB955 C061CAB1 AFC29DD7 66CE9D6E 
  7B0B84DC E9172E0D F3351141 1999F9
        quit
cts server deadtime 0
no cts server test all enable
cts server test all idle-time 0
cts server test all deadtime 0
archive
 path ftp://3.3.3.2/$h.cfg
 write-memory
!
!
!
!
spanning-tree mode pvst
spanning-tree extend system-id
no spanning-tree vlan 10,20,100
!
!
!
!
!
!
vlan internal allocation policy ascending
!
vlan 10
 name TO-Equinix-BGP-Fibre
!
vlan 11
 name TO-Equinix-BGP-BAK
!
vlan 12
 name EQX-PEERING
!
vlan 20
 name TO-NovaTel-Customer
!
vlan 100
 name MGMT
!
vlan 102
 name EQUINIX-P2P
!
vlan 103
 name Public-Management
!         
vlan 104-105 
!
vlan 120
 name Flagexplore
!
vlan 121
 name Megaport-test
!
vlan 300 
!
vlan 413
 name ACQ_PEER-MP1252
!
vlan 420
 name OPTI_NBN_CVC
!
vlan 1504,1600,1700,1800 
!
ip ftp source-interface Vlan103
ip ftp username configbackup
ip ftp password NET888cube
!
class-map match-all Flagexplore-UPLOAD
  match access-group name Flagexplore-UPLOAD
!
policy-map Flagexplore-UPLOAD
 class Flagexplore-UPLOAD
  police 2000000 500000 exceed-action drop
 class class-default
!
!
!
!
!
!
interface Port-channel1
 description PortChannel Trunk To EQX-R1
 switchport trunk encapsulation dot1q
 switchport trunk allowed vlan 10-12,20,100,102,103,120,121,413,420,1500,1600
 switchport trunk allowed vlan add 1700,1800
 switchport mode trunk
!
interface FastEthernet0
 no ip address
 shutdown
!
interface GigabitEthernet1/0/1
 description Uplink To EQX-R1 (Gig 1/0/1) [Po1]
 switchport trunk encapsulation dot1q
 switchport trunk allowed vlan 10-12,20,100,102,103,120,121,413,420,1500,1600
 switchport trunk allowed vlan add 1700,1800
 switchport mode trunk
 channel-group 1 mode on
!
interface GigabitEthernet1/0/2
 description Uplink To EQX-R1 (Gig 1/0/2) [Po1]
 switchport trunk encapsulation dot1q
 switchport trunk allowed vlan 10-12,20,100,102,103,120,121,413,420,1500,1600
 switchport trunk allowed vlan add 1700,1800
 switchport mode trunk
 channel-group 1 mode on
!
interface GigabitEthernet1/0/3
 description Uplink To EQX-R1 (Gig 1/0/3) [Po1]
 switchport trunk encapsulation dot1q
 switchport trunk allowed vlan 10-12,20,100,102,103,120,121,413,420,1500,1600
 switchport trunk allowed vlan add 1700,1800
 switchport mode trunk
 channel-group 1 mode on
!
interface GigabitEthernet1/0/4
 description Uplink To EQX-R1 (Gig 1/0/4) [Po1]
 switchport trunk encapsulation dot1q
 switchport trunk allowed vlan 10-12,20,100,102,103,120,121,413,420,1500,1600
 switchport trunk allowed vlan add 1700,1800
 switchport mode trunk
 channel-group 1 mode on
!
interface GigabitEthernet1/0/5
 description Uplink To EQX-R1 (Gig 1/0/5) [Po1]
 switchport trunk encapsulation dot1q
 switchport trunk allowed vlan 10-12,20,100,102,103,120,121,413,420,1500,1600
 switchport trunk allowed vlan add 1700,1800
 switchport mode trunk
 channel-group 1 mode on
!
interface GigabitEthernet1/0/6
 description Flagcentre-Port1
 switchport access vlan 120
 switchport mode access
 service-policy input Flagexplore-UPLOAD
!
interface GigabitEthernet1/0/7
 description Flag S1-Port1
 switchport access vlan 120
 switchport mode access
 service-policy input Flagexplore-UPLOAD
!
interface GigabitEthernet1/0/8
 description Flag S2-Port1
 switchport access vlan 120
 switchport mode access
 service-policy input Flagexplore-UPLOAD
!
interface GigabitEthernet1/0/9
 description Flagcentre-idrac
 switchport access vlan 120
 switchport mode access
 service-policy input Flagexplore-UPLOAD
!
interface GigabitEthernet1/0/10
 description Flag S1-idrac
 switchport access vlan 120
 switchport mode access
 service-policy input Flagexplore-UPLOAD
!
interface GigabitEthernet1/0/11
 description Flag S2-idrac
 switchport access vlan 120
 switchport mode access
 service-policy input Flagexplore-UPLOAD
!
interface GigabitEthernet1/0/12
 description Ezytrail
 switchport access vlan 121
 switchport mode access
 spanning-tree bpduguard enable
!
interface GigabitEthernet1/0/13
!
interface GigabitEthernet1/0/14
!
interface GigabitEthernet1/0/15
!
interface GigabitEthernet1/0/16
!
interface GigabitEthernet1/0/17
!
interface GigabitEthernet1/0/18
!
interface GigabitEthernet1/0/19
!
interface GigabitEthernet1/0/20
!         
interface GigabitEthernet1/0/21
!
interface GigabitEthernet1/0/22
!
interface GigabitEthernet1/0/23
!
interface GigabitEthernet1/0/24
!
interface GigabitEthernet1/1/1
 description Provider - Equinix - Transit AS23686 (Backup)
 switchport access vlan 11
 switchport mode access
 speed nonegotiate
!
interface GigabitEthernet1/1/2
 description Provider - Megaport - P2P AGG (CID:1252)
 switchport trunk encapsulation dot1q
 switchport trunk allowed vlan 413,420
 switchport mode trunk
 switchport nonegotiate
 spanning-tree bpdufilter enable
!
interface GigabitEthernet1/1/3
 description Provider - Equinix Peering [AS24115]
 switchport access vlan 12
 switchport mode access
!
interface GigabitEthernet1/1/4
!
interface TenGigabitEthernet1/1/1
!
interface TenGigabitEthernet1/1/2
!
interface Vlan1
 no ip address
 shutdown 
!
interface Vlan32
 description server-management
 no ip address
!
interface Vlan100
 description Management (Private)
 ip address 192.168.1.2 255.255.255.0
!
interface Vlan101
 no ip address
!
interface Vlan103
 description Management (Public)
 ip address 43.252.111.20 255.255.255.248
!
interface Vlan121
 description Megaport-test
 ip address 172.16.0.2 255.255.255.252
 shutdown
!
interface Vlan300
 no ip address
!
interface Vlan420
 description Opticomm-Test
 ip address 172.16.0.2 255.255.255.252
 shutdown
!
interface Vlan1504
 ip address 192.168.4.3 255.255.255.0
!
ip default-gateway 43.252.111.19
ip http server
ip http secure-server
ip route 192.168.1.0 255.255.255.0 192.168.4.1
ip route 192.168.3.0 255.255.255.0 192.168.4.1
!
ip access-list extended Flagexplore-UPLOAD
 permit ip 103.26.62.80 0.0.0.15 any
ip access-list extended Flagexplore-uPLOAD
 permit ip 103.26.62.112 0.0.0.15 any
kron occurrence BACKUP_OCCURRENCE at 23:00 recurring
 policy-list BACKUP
!
kron policy-list BACKUP
 cli write memory
!
access-list 17 permit 203.111.96.209
access-list 17 permit 120.149.176.111
access-list 17 permit 1.136.97.95
access-list 17 permit 14.137.113.77
access-list 17 permit 54.252.97.16
access-list 17 permit 192.168.0.0 0.0.255.255
access-list 17 permit 14.137.150.0 0.0.0.255
access-list 17 permit 43.252.108.0 0.0.3.255
access-list 17 permit 103.26.60.0 0.0.3.255
access-list 17 permit 103.58.216.0 0.0.3.255
access-list 17 permit 103.249.140.0 0.0.3.255
access-list 17 permit 103.18.48.0 0.0.3.255
access-list 42 permit 192.168.3.0 0.0.0.255
snmp-server group netcubeuser v3 auth notify *tv.FFFFFFFF.FFFFFFFF.FFFFFFFF.FFFFFFFF.FFFFFFFF.FFFFFFFF.FFFFFFFF.FFFFFFFF.FFFFFFFF.FFFFFFFF.FFFFFFFF.FFFFFFFF.FFFFFFFF.FFFFFFFF.FFFFFFFF0F 
snmp-server group netcubegroup v3 auth read netcuberead write netcubewrite notify *tv.FFFFFFFF.FFFFFFFF.FFFFFFFF.FFFFFFFF.FFFFFFFF.FFFFFFFF.FFFFFFFF.FFFFFFFF.FFFFFFFF.FFFFFFFF.FFFFFFFF.FFFFFFFF.FFFFFFFF.FFFFFFFF.FFFFFFFF7F 
snmp-server group netcube-cisco-v3-group v3 auth read netcube-cisco-v3-read write netcube-cisco-v3-write notify *tv.FFFFFFFF.FFFFFFFF.FFFFFFFF.FFFFFFFF.FFFFFFFF.FFFFFFFF.FFFFFFFF.FFFFFFFF.FFFFFFFF.FFFFFFFF.FFFFFFFF.FFFFFFFF.FFFFFFFF.FFFFFFFF.FFFFFFFF7F 
snmp-server view netcube iso included
snmp-server view netcuberead iso included
snmp-server view netcubewrite iso included
snmp-server view netcube-cisco-v3-read iso included
snmp-server view netcube-cisco-v3-write iso included
snmp-server community IXmm8K6lFp RO snmp-in
snmp-server community H05t8IkkW79TE RO snmp-in
snmp-server community netcube RW netcuberw
snmp-server community private RW
snmp-server community public RO
snmp-server community NETCUBEread RO 42
snmp-server enable traps snmp authentication linkdown linkup coldstart warmstart
snmp-server enable traps flowmon
snmp-server enable traps transceiver all
snmp-server enable traps call-home message-send-fail server-fail
snmp-server enable traps tty
snmp-server enable traps license
snmp-server enable traps auth-framework sec-violation
snmp-server enable traps cluster
snmp-server enable traps config-copy
snmp-server enable traps config
snmp-server enable traps config-ctid
snmp-server enable traps dot1x auth-fail-vlan guest-vlan no-auth-fail-vlan no-guest-vlan
snmp-server enable traps energywise
snmp-server enable traps fru-ctrl
snmp-server enable traps entity
snmp-server enable traps event-manager
snmp-server enable traps power-ethernet police
snmp-server enable traps cpu threshold
snmp-server enable traps ipsla
snmp-server enable traps vstack
snmp-server enable traps bridge newroot topologychange
snmp-server enable traps stpx inconsistency root-inconsistency loop-inconsistency
snmp-server enable traps syslog
snmp-server enable traps vtp
snmp-server enable traps vlancreate
snmp-server enable traps vlandelete
snmp-server enable traps flash insertion removal
snmp-server enable traps port-security
snmp-server enable traps envmon fan shutdown supply temperature status
snmp-server enable traps stackwise
snmp-server enable traps errdisable
snmp-server enable traps mac-notification change move threshold
snmp-server enable traps vlan-membership
snmp-server host 192.168.3.168 version 3 auth netcube-cisco-v3-user 
snmp-server host 192.168.3.156 version 3 auth netcubeuser 
!
!
line con 0
 logging synchronous
 login local
line vty 0 4
 access-class 17 in vrf-also
 logging synchronous
 login local
 transport input ssh
line vty 5 15
 access-class 17 in vrf-also
 logging synchronous
 login local
 transport input ssh
!
!
monitor session 1 source interface Gi1/1/1 tx
monitor session 1 destination interface Gi1/0/12
ntp server 202.127.210.36
end

